/*
1. Чому для роботи з input не рекомендується використовувати клавіатуру?
   Інформація в поле може вводитись не лише з використанням клавіатури - вставка тексту, екранні клавіатури, голосове введення та ін.
   Можна відслідковувати значення value поля input при кожній зміні за допомогою івенту input.

*/

const btnCollection = document.querySelectorAll(".btn");

function keyPainter (event) {
  btnCollection.forEach (function (el) {
    el.classList.remove("active");
      if (el.dataset.key === event.code) {
        el.classList.add("active");
      } 
  });
}

document.addEventListener('keydown', keyPainter);



